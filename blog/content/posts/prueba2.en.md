---
title: Tools
date: 2020-06-10T21:39:07Z
lastmod: 2020-06-10T21:39:07Z
author: Tedic
cover: /img/cover.png
categories: ["Freedom of expression"]
tags: ["drones", "tool"]
# showcase: true
#draft: true
---

This is a short contexto

<!--more-->

Ingles

Muchas veces, dada la urgencia de la situación por la que pasan las instituciones educativas al tener que migrar a una modalidad virtual, se optan por “soluciones” o plataformas que son privativas y pagas. Esto, sin muchas veces tener en cuenta de que existen recursos y herramientas para la educación virtual de acceso libre y basadas en el conocimiento compartido. Por ende, desde TEDIC queremos compartir contigo sobre la importancia de conocer y utilizar herramientas y recursos de este tipo, sugiriendo también algunas que son alternativas a las privativas, y que aportan al acceso a la información y al conocimiento abierto, considerando que pueden ser útiles tanto para profesores, padres, y jóvenes con intereses diversos.
El qué y el por qué de la educación y el conocimiento abierto

La evolución de Internet dio lugar a una variedad de espacios de comunicación, acceso a la información y aprendizaje; muchos de ellos de acceso pago y restringido, pero también muchos otros de acceso abierto y basados en el conocimiento compartido, con licencias y formatos técnicos abiertos que facilitan el uso, la descarga, edición y redistribución de los mismos.

Estos recursos se apoyan en su nivel de apertura legal definido a través de algunas libertades concedidas por los autores de las mismas, conocidas como las 5Rs, según el investigador David Wiley y la comunidad de Creative Commons:

    Reutilizar: la libertad de usar la obra original en distintos contextos;
    Revisar: la libertad de adaptar, modificar o traducir para que la obra o recurso sea adecuado a la necesidad o al contexto;
    Remixar: la libertad de combinar y mezclar distintos recursos con libertades similares para así crear nuevos materiales;
    Redistribuir: la libertad de compartir el recurso original y la versión remixada;
    Retener: la libertad de copiar y descargar el recurso en cualquier dispositivo.

Utilizar y contar con recursos de este tipo permite el aumento el conocimiento de manera colectiva, uniendo bloques pertenecientes a distintas personas y comunidades, para ir dando forma a distintas maneras de acceso a la información y el conocimiento basadas en procesos de apertura y colaboración.
Algunas herramientas y plataformas

Por más de que deseemos hacerlo, este post sería interminable si nos ponemos a citar todas las herramientas y recursos abiertos disponibles para la educación; por ende, nos gustaría empezar comentándote sobre las más populares y también sobre otras que encontramos en el camino, que poseen contenido en español (no solamente en inglés) y que nos parecieron interesantes mencionar:
Wikipedia

Aparte de ser una de las plataformas más populares en la web, Wikipedia es la enciclopedia digital más grande del mundo, con artículos publicados en una diversidad de idiomas y sobre distintos temas. Wikipedia esta alojada por la Fundación Wikimedia, una organización sin fines de lucro que también tiene otros proyectos, como por ejemplo:

    Wikimedia Commons: Fotos con licencia abierta que pueden ser utilizadas gratuitamente;
    Wikidata: Bases de datos abiertas que pueden ser leídas y editadas libremente;
    Wikiversity: Recursos académicos libres;
    Wikibooks: Libros de acceso libre y gratuito.

Y muchas otras iniciativas más que van de mano con los esfuerzos del acceso libre al conocimiento. Compartimos también la plataforma de recursos abiertos realizada por la comunidad de Wikimedia Argentina, y también todo lo relacionado a nuestras actividades en torno a Wikipedia acá.
Internet Archive

Internet Archive es una biblioteca digital creada por una organización sin fines de lucro que contiene millones de libros, software, música, sitios web películas, imágenes y mucho más, inclusive un archivo de 424 billones de sitios web disponibles de manera gratuita. Sumando a estos recursos libres con los que cuenta, la plataforma tomó una decisión significativa alproveer una biblioteca nacional de emergencia para todos los estudiantes de Estados Unidos en estos tiempos de pandemia. Esto significa que agregaron 1.4 millones de libros a su colección desde el 24 de marzo, facilitando así el acceso a una inmensa cantidad de contenido útil para estudiantes y usuarios interesadxs. La misma permanecerá abierta hasta que termine el periodo de emergencia.

Esta biblioteca es IN-MEN-SA, dando la posibilidad de tener una variedad incontable de recursos disponibles con tan solo unos pocos clicks. Acá te compartimos nuestra biblioteca con todos nuestros materiales, y también, algunos sitios favoritos de la autora de este post: archivo de videojuegos de arcade de los 70s-90s: y de videojuegos de MS-DOS que se pueden jugar directamente a través de un emulador en el sitio.
Story weaver

Una plataforma que si bien está en inglés, posee más de 400 historias traducidas al español y programas de lectura para niños, niñas y jóvenes desde el 1er al 8vo grado, traducidas al español. El acceso a estos materiales es libre, y se pueden crear bibliotecas personales tanto en línea como fuera de línea (para esto último es necesario crearse un perfil, lo cual tampoco tiene costo alguno)

Todas las historias se encuentran con licencia CC-BY 4.0. También brinda la opción de traducción para personas con el interés en colaborar y traducir historias en la plataforma, para así aumentar el nivel de materiales disponibles a la comunidad. Las traducciones se pueden realizar tanto en línea como fuera de línea.
El directorio de revistas e investigaciones de acceso abierto (Directory of Open Access Journals – DOAJ)

Según lo indicado en su sitio, DOAJ es un directorio en línea curado por la comunidad que indexa y proporciona acceso a revistas de alta calidad, acceso abierto y revisadas por pares. DOAJ es independiente, y toda la financiación se realiza a través de donaciones de parte de patrocinadores, miembros y miembros de editor . Todos los servicios de DOAJ y datos son gratuitos, incluido el indexado en DOAJ. Podés acceder al sitio acá.
MIT BLOSSOMS (Blended Learning Open Source Science or Math Studies)

Este es un repositorio alojado por el Massachusetts Institute of Technology – MIT que contiene cursos y materiales didácticos abiertos de biología, química, ingeniería, física y matemáticas disponibles en distintos idiomas para educación secundaria. Cada curso posee guías para los profesores y también herramientas de evaluación para descargar de manera gratuita, además de recursos adicionales para complementar el contenido de los mismos. La mayoría de los recursos tienen también licencia de CC-BY 3.0. Podés acceder al sitio acá.

También, para tener acceso a los cursos universitarios del MIT de manera abierta y gratuita, te recomendamos visitar su plataforma MIT OpenCourseware.
Kiwix

Kiwix es una aplicación que te permite acceder a recursos educativos abiertos, como Wikipedia, Wiktionary y otros, sin contar con una conexión a Internet. Tras instalar la aplicación, te permite descargar el contenido deseado desde el sitio web de Kiwix. Luego, ese contenido puede ser accedido sin una conexión de Internet. También, el contenido puede ser colocado en algún dispositivo, para moverlo de un lugar a otro. Facilita el acceso a la información en lugares en donde el acceso y la conexión a Internet son muy difíciles de conseguir.

Tanto la aplicación móvil como la aplicación para computadoras se encuentran disponibles para distintas plataformas:

    Sitio web: Kiwix.org
    Aplicación de escritorio: Windows / macOS / Linux
    Aplicación móvil:
        Android (FDroid)
        iOS

Alternativas sonoras

Es importante reconocer que en Paraguay contamos con un porcentaje significativo que refleja la falta de asequibilidad y conectividad de Internet, lo cual también lleva a utilizar otros formatos tanto como para educar y acceder a la información, comprendiendo que muchxs no tienen acceso a recursos en línea. La creación de podcasts (grabación de audios que se pueden difundir por diversos medios y canales de comunicación) pueden ser una alternativa no solamente ante dicha situación, sino también pueden ser un apoyo para estudiantes y personas que cuentan con discapacidad visual. Aquí te compartimos algunos recursos para explorar:

    Canales de podcasts educativos: https://www.educaciontrespuntocero.com/recursos/los-mejores-podcasts-educativos/
    Producciones libres de todo tipo de contenido: https://radioslibres.net/producciones-libres-y-gratis/
    Nuestros episodios de podcast de Radio Cyborg también se encuentran disponibles bajo licencia de CC-BY 4.0 🙂
    ¿Querés hacer tu propio canal de podcast? Acá te compartimos un repositorio de recursos libres realizado por la comunidad de Radios Libres para hacerlo: https://radioslibres.net/
    Para edición y producción de audios, te recomendamos también la plataforma Audacity, que es de software libre, disponible para MAC, Linux y Windows, y es gratuita.
    ¿Querés aventurarte más en el mundo radial desde tu casa? Acá te compartimos este increíble manual para empezar de cero.

Esperamos que te animes a explorar cada una estas plataformas y que te sean de utilidad. Sabemos que no es fácil deshacernos de muchos de nuestros hábitos digitales, aún mucho más si son vistos cómo soluciones rápidas y prácticas usadas por la mayoría, pero siempre es importante seguir analizando y probando alternativas a lo privativo.

¿No encontraste algo que te sirva o interese en este post? Podés intentar buscando en sitios que detallan alternativas a programas para negocios, coordinación de proyectos, diseño, y mucho más. Aquí compartimos algunas:

    Opensource.com
    Switching.software
    Alternativeto.net

Si querés saber más sobre educación y cultura abierta, te recomendamos que visites el sitio de Ártica Online, un centro cultural online basado en Uruguay, que posee un montón de recursos e información sobre el tema, así como también oportunidades de capacitación. También, te recomendamos que visites el sitio web de CCSearch para tener acceso a un montón de recursos con la licencia de Creative Commons, y también el sitio de Creative Commons en Paraguay para enterarte de todas las actividades realizadas, aprender sobre el uso de este tipo de licencia, y sumarte a la comunidad.

¿Conocés alguna plataforma o herramienta de recursos abiertos? ¿Encontraste alguna página o material dentro de las herramientas que compartimos que te gustaría sugerir?

Compartíla a través de tus comentarios o escribínos a hola@tedic.org 🙂
