---
title: Salud mental y prácticas de cuidados digitales en tiempos de COVID19
date: 2020-06-10T21:39:07Z
lastmod: 2020-06-10T21:39:07Z
author: Tedic
cover: /img/salud_mental.png
categories: ["Libertad de expresión"]
tags: ["drones", "app", "herramienta", "género"]
# showcase: true
#draft: true
---

[Salud mental y prácticas de cuidados digitales en tiempos de #COVID19](https://www.tedic.org/salud-mental-covid19/)

<!--more-->
