---
title: Herramientas de conocimiento abierto para la educación virtual
date: 2020-06-09T21:39:07Z
lastmod: 2020-06-10T21:39:07Z
author: Tedic
cover: /img/cover.png
categories: ["Libertad de expresión"]
tags: ["drones", "app", "herramienta"]
# showcase: true
#draft: true
---

Esto es otro artículo

<!--more-->

En el primer posteo sobre Educación en tiempos de #COVID19 parte de nuestra serie Antivirus, hablamos con mayor detalle sobre los desafíos y las limitaciones encontradas ante la compleja situación de educación virtual en nuestro país y de cómo la privatización del sector educativo amplía cuestiones como la brecha digital existente.
